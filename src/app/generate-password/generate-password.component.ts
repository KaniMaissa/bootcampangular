import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-generate-password',
  templateUrl: './generate-password.component.html',
  styleUrls: ['./generate-password.component.css']
})
export class GeneratePasswordComponent implements OnInit {

  ngOnInit(): void {
    
  }

  includeLetters=false;
  includeSymboles=false;
  includeNumbers=false;
  password='';
  length=0;

  onChangeLength(event:any){
  console.log("event",event);
    const parseValue=parseInt(event.target.value);
    if(!isNaN(parseValue)){
      this.length=parseValue;
      console.log('length',this.length);
    }
    console.log(parseValue);
  }

  onChangeNumbers(){
  this.includeNumbers=!this.includeNumbers;
  }

  onChangeSymbols(){
    this.includeSymboles=!this.includeSymboles;
  }
  onChangeLetters(){
    this.includeLetters=!this.includeLetters;
  }
  onButtonClick(){
  const numbers='1234567890';
  const letters='abcdefghijklmnopqrstuvwyz';
  const symboles='!@#$%^&*()';
  let validChars = '';

    if(this.includeNumbers){
      validChars+=numbers;
    }
    if(this.includeLetters){
      validChars+=letters
    }
    if(this.includeSymboles){
      validChars+=symboles
    }
    let generatedPassword = '';

    for(let i=0;i <this.length;i++){
      const index = Math.floor(Math.random()*validChars.length);
      console.log('idex',index);
      generatedPassword+=validChars[index];
    }
    this.password=generatedPassword;
  }
}
