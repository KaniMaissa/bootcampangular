import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes-form',
  templateUrl: './pipes-form.component.html',
  styleUrls: ['./pipes-form.component.css']
})
export class PipesFormComponent implements OnInit {
  name:string;
  date:string;
  amount:number;
  height:number;
  mile:number;
  car ={
    make:"Tokyo",
    model:'Camry',
    year:2000
  };
  constructor() { }

  ngOnInit(): void {
  }

  onNameChange(value:any){
    this.name=value.target.value;
  }

  onDateChange(value:any){
    this.date=value.target.value;
  }

  onAmountChange(value:any){
    this.amount=value.target.value;
  }

  onHeightChange(value:any){
    this.height=value.target.value;
  }
  onMileChange(value:any){
    this.mile=value.target.value;
  }
}
