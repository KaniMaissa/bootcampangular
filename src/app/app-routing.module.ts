import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { GeneratePasswordComponent } from './generate-password/generate-password.component';
import { CardsComponent } from './cards/cards.component';
import { TypingComponent } from './typing/typing.component';
import { PipesFormComponent } from './pipes-form/pipes-form.component';
import { PagesComponent } from './pages/pages.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';

const routes:Routes = [
  {path:'elements',loadChildren:() => import ('./elements/elements.module').then((m) => m.ElementsModule)},
  {path:'collections',loadChildren:() => import ('./collections/collections.module').then((m) => m.CollectionsModule)},
  {path:'views',loadChildren:() => import ('./views/views.module').then((m) => m.ViewsModule ) },

  {path:'generatePassword',component:GeneratePasswordComponent},
  {path:'cards',component:CardsComponent},
  {path:'typing',component:TypingComponent},
  {path:'pipe',component:PipesFormComponent},
  {path:'pages',component:PagesComponent},
  {path:'',component:HomeComponent},
  {path:'**',component:NotFoundComponent},

]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports:[RouterModule]
})


export class AppRoutingModule { }
