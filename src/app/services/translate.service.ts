import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class TranslateService {
  url='https://translate.googleapis.com/language/translate/';
  constructor(private http:HttpClient) {

   }
   translate(text:string){
    return this.http.post(this.url,{"q":text,"target":"es"}).pipe(
      map((res:any)=>{
        return res.data.translations[0].translatedText;
      })
    )
   }
}
