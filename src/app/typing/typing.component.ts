import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-typing',
  templateUrl: './typing.component.html',
  styleUrls: ['./typing.component.css']
})
export class TypingComponent implements OnInit {
  enteredText='';

  randomText="Randomly generated text should go here";
  solved=false;
  constructor() { }

  ngOnInit(): void {
  }

  onInput(value:any){
    this.enteredText=value.target.value;

  }

  compare(randomLettre:string,enteredLetter:string){
    if( !enteredLetter){
      return 'pending';
    }
    return randomLettre === enteredLetter ? 'correct' : 'incorrect';
  }
}
