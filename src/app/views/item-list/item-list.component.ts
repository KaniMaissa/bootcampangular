import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {
  items = [
    {value:22, label: '# of Users'},
    {value:900, label:'Revenue'},
    {value:50 ,label :'Reviews'}
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
