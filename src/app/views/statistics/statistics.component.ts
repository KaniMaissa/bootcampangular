import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {

  stats = [
    {value:22, label: '# of Users'},
    {value:900, label:'Revenue'},
    {value:50 ,label :'Reviews'}
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
