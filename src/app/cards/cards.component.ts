
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {

    posts=[
      {
        title:'Neat Tree',
      username:'nature',
      content:' Saw this awsome tree during my hike today.',
      imageUrl:'../../assets/images/tree.jpeg'
      },
      {
        title:'Snowy Mountain',
      username:'Mountain',
      content:' Here is a picture  of a snowy mountain.',
      imageUrl:'../../assets/images/mountain.jpeg'
      },
      {
        title:'Mountain Biking',
      username:'biking1222',
      content:' I did some biking today.',
      imageUrl:'../../assets/images/biking.jpeg'
    }
    ]
  constructor() { }

  ngOnInit(): void {
  }

}
