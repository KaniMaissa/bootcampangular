import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-collections-home',
  templateUrl: './collections-home.component.html',
  styleUrls: ['./collections-home.component.css']
})
export class CollectionsHomeComponent implements OnInit {
 partOfLink="hi";
  data= [
    {name:'maissa',age:28,job:'designer',employed:true},
    {name:'maissa',age:16,job:'designer',employed:false},
    {name:'maissa',age:32,job:'designer',employed:true},];

  headers= [

    {key:'employed',label:'Has A job'},
    {key:'name',label:'Name'},
    {key:'age',label:'Age'},
    {key:'job',label:'Job'}];

  constructor() { }

  ngOnInit(): void {
  }

}
