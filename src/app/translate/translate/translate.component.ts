import { Component, OnInit } from '@angular/core';
import { TranslateService } from 'src/app/services/translate.service';

@Component({
  selector: 'app-translate',
  templateUrl: './translate.component.html',
  styleUrls: ['./translate.component.css']
})
export class TranslateComponent implements OnInit {
  text:string;
  texttranslated:string;
  constructor(private readonly translateService:TranslateService) { }

  ngOnInit(): void {
  }

  submit(){
    this.translateService.translate(this.text).subscribe((res)=>{
      this.texttranslated=res;
    })
  }

}
