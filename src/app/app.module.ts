import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { GeneratePasswordComponent } from './generate-password/generate-password.component';
import { AppRoutingModule } from './app-routing.module';
import { CardsComponent } from './cards/cards.component';
import { CardComponent } from './cards/card/card.component';
import { TypingComponent } from './typing/typing.component';
import { PipesFormComponent } from './pipes-form/pipes-form.component';
import { ConvertPipe } from './convert.pipe';
import { PagesComponent } from './pages/pages.component';
import { ClassDirective } from './class.directive';
import { TimesDirective } from './times.directive';
import { CollectionsModule } from './collections/collections.module';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    GeneratePasswordComponent,
    CardsComponent,
    CardComponent,
    TypingComponent,
    PipesFormComponent,
    ConvertPipe,
    PagesComponent,
    ClassDirective,
    TimesDirective,
    HomeComponent,
    NotFoundComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
